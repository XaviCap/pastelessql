package PracticaJDBC.Projecte;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Scanner;

import Queries.Consulta;

public class App {
	
	
	public static void main( String[] args )
    {
       
    	System.out.println("0) Exit"+"\n"+"1) Veure taula escollida"+"\n"+"2) Veure taules"+"\n"+"3) Crear comanda"+"\n"+"4) Veure comanda per client"+"\n"+"5) Generar informe"+"\n"+"7) Preparar comanda"+"\n"+"8) Generar ordre"+"\n"+"9) Veure ordre"+"\n"+"10) Veure Diari Moviments ");
        
        Scanner sc = new Scanner(System.in);
      
        while(true) {
        	
        	int n = sc.nextInt();
        
        switch(n) {
        
        case 0:
        	
        	System.exit(0);
        	
        	break;
        	
        case 1:
        	
        	String s = sc.next();
        	
        	Consulta.veurequalesvolcosa(s);
        
        	break;
        
        case 2:
        	
        	Consulta.veuretablesycolumnes();
        	
        	break;
        	
        
        case 3:
        	Consulta.consultarClients();
        	
        	System.out.println("introdueixi un codi de client");
        	int cl = sc.nextInt();
        	
        	Consulta.crearComanda(cl);
        	
        	Consulta.veureProducteVendible();
        	
        	System.out.println("introdueixi un codi Producte");
        	int prod = sc.nextInt();
        	System.out.println("introdueixi una quantitat");
        	int pr = sc.nextInt();
        	System.out.println("introdueixi un preu");
        	int preu = sc.nextInt();
        	
        	Consulta.newLinea(pr, preu, prod);
        	
        	break;
        
        
        case 4:
        	Consulta.consultarClients();
        	System.out.println("introdueixi un codi de client");
        	int cl2 = sc.nextInt();
        	Consulta.vercomanda(cl2);
        	System.out.println("introdueixi un codi de comanda");
        	int com = sc.nextInt();
        	Consulta.vercomandalinia(com);
        	Consulta.veurepreuVenda(com);
        	break;
        	
        case 5:
        	
        	if(Consulta.borrarInforme()>0) {
        		
        		Consulta.createInforme();
        		
        	};
        	break;
        	
        case 7:
        	
        	Consulta.veurePendents();
        	
        	//Consulta.prepararComanda();
        	break;
        case 8:
        	
        	Consulta.generarOrdres();
        	break;
        case 9:
        	
        	Consulta.veureOrdres();
        	break;
        case 10:
        	
        	Consulta.veureDiari();
        	break;
        }
    }}
}
