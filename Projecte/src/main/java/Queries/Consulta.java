package Queries;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.omg.Messaging.SyncScopeHelper;

import com.mysql.cj.jdbc.DatabaseMetaDataUsingInfoSchema;
import com.mysql.cj.x.protobuf.MysqlxCrud.Column;

import PracticaJDBC.Projecte.App;

public class Consulta {

	public static ResultSet vercomandalinia(int com) {

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {
			Statement st = con.createStatement();
			try (ResultSet rs = con.createStatement()
					.executeQuery("SELECT * FROM comanda_linia where idComanda = " + com)) {
				System.out.println("idComanda	idlinia	idProducte	quantitat	preuVenda ");
				String su = "";
				while (rs.next()) {

					for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {

						if (rs.getObject(i) != null) {
							su += rs.getObject(i).toString() + "\t";
						} else {
							su += "nul" + "\t";
						}

					}

					su += "\n";

				}

				System.out.println(su);

				return rs;

			}

		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			return null;

		}

	}

	public static void veurepreuVenda(int com) {
		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {
			Statement st = con.createStatement();
			try (ResultSet rs1 = con.createStatement()
					.executeQuery("SELECT sum(preuVenda) FROM comanda_linia where idComanda = " + com)) {

				System.out.println("preuVenda total ");

				if (rs1.next()) {
					int n = rs1.getInt(1);
					System.out.println(n);

				}

			}
		}

		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	public static void vercomanda(int cl2) {

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {
			Statement st = con.createStatement();
			try (ResultSet rs = con.createStatement().executeQuery("SELECT * FROM comanda where idClient = " + cl2)) {

				System.out.println("idComanda	idClient	dataComanda	dataLluirament	estatComanda	ports");
				String su = "";
				while (rs.next()) {

					for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {

						if (rs.getObject(i) != null) {
							su += rs.getObject(i).toString() + "\t";
						} else {
							su += "nul" + "\t";
						}

					}

					su += "\n";

				}

				System.out.println(su);
			}

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public static void veureProducteVendible() {

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {
			Statement st = con.createStatement();
			try (ResultSet rs = con.createStatement().executeQuery(
					"SELECT idProducte, nomProducte, preuVenda, stock FROM producte where tipusProducte = 'VENDIBLE'")) {

				System.out.println("id	nom		preu	stock");

				while (rs.next()) {

					int idP = rs.getInt("idProducte");
					String nomP = rs.getString("nomProducte");
					int preu = rs.getInt("preuVenda");
					int stock = rs.getInt("stock");

					System.out.println(idP + "\t" + nomP + "\t" + preu + "\t" + stock);
				}
			}

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/* 1 */

	public static void veurequalesvolcosa(String s) {// no tiene sentido

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {

			Statement st = con.createStatement();
			String su = "";
			String st2 = "SELECT * FROM " + s + "";
			st.executeQuery(st2);
			ResultSet rs = st.executeQuery(st2);
			{
				DatabaseMetaData dt = con.getMetaData();

				ResultSet columns = dt.getColumns(null, null, s, null);

				String suuuu = "";

				while (columns.next()) {
					String columnName = columns.getString("COLUMN_NAME");

					suuuu += columnName + "\t";

				}
				System.out.println(suuuu);
				while (rs.next()) {

					for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {

						if (rs.getObject(i) != null) {
							su += rs.getObject(i).toString() + "\t";
						} else {
							su += "nul" + "\t";
						}

					}

					su += "\n";

				}

			}

			System.out.println(su);

		} catch (

		SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void meteralgo() {

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {
			Statement st = con.createStatement();
			/*
			 * String
			 * meter="INSERT INTO Producte(idProducte,nomProducte,descripcioProducte,preuVenda,stock,stockMinim,tipusProducte,UnitatMesura,idProveidor,tempsFabricacio) values(200000,'Cadaveres','huele a muerto',1,100,10,'VENDIBLE','UNITAT',1,1)"
			 * ; st.execute(meter);
			 */
			System.out.println("te la han colao");
			ResultSet rs = st.executeQuery("SELECT * FROM  Producte WHERE nomProducte='Cadaveres'");
			{

				while (rs.next()) {

					int idP = rs.getInt("idProducte");
					String nomP = rs.getString("nomProducte");
					String unitat = rs.getString("UnitatMesura");
					int idProv = rs.getInt("idProveidor");
					int stock = rs.getInt("stock");
					int stockMin = rs.getInt("StockMinim");
					System.out.println(
							idP + "\t" + nomP + "\t" + unitat + "\t" + idProv + "\t" + stock + "\t" + stockMin);
				}
			}

			System.out.println(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void borrarProducto() {// deleting code

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {
			Statement st = con.createStatement();
			String st2 = "DELETE FROM Producte WHERE nomProducte ='pLiviano' ";
			st.executeUpdate(st2);
			System.out.println("adios nabius");
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	/* 2 */

	public static void veuretablesycolumnes() {

		ArrayList<String> tables = new ArrayList<String>();

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {

			Statement st = con.createStatement();
			Statement st2 = con.createStatement();

			System.out.println("Base de dades connectada!");
			try (ResultSet rs = st.executeQuery("show tables")) {

				String s = "";

				while (rs.next()) {

					String table = rs.getString("tables_in_ttb");

					tables.add(rs.getString("tables_in_ttb"));
					s = s + rs.getString("tables_in_ttb");

					try (ResultSet rs2 = st2.executeQuery("DESCRIBE " + table + "")) {

						while (rs2.next()) {

							s = s + "\t" + rs2.getString("Field");
							s = s + "\t" + rs2.getString("Type");

						}

					}

					s = s + "\n";

				}

				System.out.println(s);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/* 3 */

	public static void consultarClients() { /* 3.1 */

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {

			Statement st = con.createStatement();

			String str = "select idClient, nomClient from client";

			try (ResultSet rs = st.executeQuery(str)) {

				String s = "codi	nom" + "\n";

				while (rs.next()) {

					s = s + rs.getString(1) + "	" + rs.getString(2);

					s = s + "\n";

				}

				System.out.println(s);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void crearComanda(int cl) {

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {

			Statement st = con.createStatement();

			Date today = new Date();
			SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
			String date = DATE_FORMAT.format(today);
			System.out.println("Today in yyyy-MM-dd format : " + date);

			String str = "insert into comanda (idClient, dataComanda, dataLliurament) values (" + cl + ",'" + date
					+ "', null)";

			int n = st.executeUpdate(str);
			System.out.println(n);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	public static void newLinea(int pr, int preu, int prod) {

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {

			Statement st = con.createStatement();
			java.util.Date fecha = new Date();

			String str = "insert into comanda_linia (idComanda, idLinia, idProducte, quantitat, preuVenda) select max(idComanda), "
					+ 1 + "," + prod + "," + pr + "," + preu + " from Comanda";

			int n = st.executeUpdate(str);
			System.out.println(n);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static int borrarInforme() {

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {

			Statement st = con.createStatement();
			java.util.Date fecha = new Date();

			String str = "drop table if exists informe";

			int n = st.executeUpdate(str);
			System.out.println(n);
			return n;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}

	}

	public static void createInforme() {

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {

			Statement st = con.createStatement();
			java.util.Date fecha = new Date();

			String str = "CREATE TABLE informe (\r\n" + "    column1 datatype,\r\n" + "    column2 datatype,\r\n"
					+ "    column3 datatype,\r\n" + "   ....\r\n" + "); ";

			int n = st.executeUpdate(str);
			System.out.println(n);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static ResultSet veurePendents() {

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {

			Statement st = con.createStatement();
			String su = "";
			String st2 = "SELECT * FROM comanda where estatComanda LIKE 'PENDENT' order by idComanda";
			st.executeQuery(st2);
			ResultSet rs = st.executeQuery(st2);
			{
				DatabaseMetaData dt = con.getMetaData();

				ResultSet columns = dt.getColumns(null, null, "comanda", null);

				String suuuu = "";

				while (columns.next()) {
					String columnName = columns.getString("COLUMN_NAME");

					suuuu += columnName + "\t";

				}
				System.out.println(suuuu);
				while (rs.next()) {

					for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {

						if (rs.getObject(i) != null) {
							su += rs.getObject(i).toString() + "\t";
						} else {
							su += "nul" + "\t";
						}

					}

					su += "\n";

				}

				System.out.println(su);
				
				prepararComanda(rs);

				return rs;

			}

		} catch (

		SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void prepararComanda(ResultSet rs) {

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {
			
			con.setAutoCommit(false);

			
			while (rs.next()) {

				ResultSet rs2 = vercomandalinia(rs.getInt(1));

				while (rs2.next()) {

					Statement st = con.createStatement();
					String su = "";
					String st2 = "SELECT (p.stock-" + rs.getInt(4)
							+ "), p.idProducte as stock_total  FROM producte p join comanda_linia cl on (p.idProducte = cl.idProducte) where cl.idLinia = "
							+ rs2.getInt(2) + "";

					ResultSet rs3 = st.executeQuery(st2);

					if (rs3.next()) {

						if (rs3.getInt(1) >= 0) {


							String str = "update producte set stock = " + rs3.getInt(1) + "where idProducte = "
									+ rs3.getInt(2);

							int updateprod = st.executeUpdate(str);

							String insertdiari = "insert into diari_moviments (idProducte,tipusMoviment,quantitat,observacions) values ("+rs3.getInt(2)+",'S',"+rs.getInt(4)+",'Se ha insertado'";

							int insert = st.executeUpdate(insertdiari);
							
						} else {

							System.out.println("no se puede preparar la comanda");
							con.rollback();
							break;

						}

					}
				}
			}

			con.commit();

			System.out.println("se ha hecho el commit");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void generarOrdres() {

		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {
			
			Statement st = con.createStatement();
			String su = "";
			String st2 = "SELECT stock, stockMinim, idProducte, idProveidor FROM producte where tipusProducte = 'INGREDIENT'";
			
			ResultSet rs = st.executeQuery(st2);
			
			while(rs.next()) {
				
				if(rs.getInt(1)<rs.getInt(2)) {
										
					String insertp = "insert into ordre_compra (idProducte, idProveidor, quantitat, dataOrdre, dataRecepcio) values("+rs.getInt(3)+","+rs.getInt(4)+","+rs.getInt(2)*2+",NOW(),NOW()+1)";
					
					System.out.println("insertado");
					
					st.executeUpdate(insertp);
					
				}
				
			}

	} catch (SQLException e) {
		// TODO Auto-generated catch block
	}
	}

	public static void veureOrdres() {
		
	
		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {
			
			Statement st = con.createStatement();
			
			String su = "";
			String st2 = "SELECT * FROM ordre_compra order by idProveidor, idProducte";
			
			ResultSet rs = st.executeQuery(st2);
			
			while (rs.next()) {

				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {

					if (rs.getObject(i) != null) {
						su += rs.getObject(i).toString() + "\t";
					} else {
						su += "nul" + "\t";
					}

				}

				su += "\n";

			}

			System.out.println(su);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void veureDiari() {
		
		
		try (Connection con = DriverManager
				.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=Henyckma_1")) {
			
			Statement st = con.createStatement();
			
			String su = "";
			String st2 = "SELECT * FROM diari_moviments";
			
			ResultSet rs = st.executeQuery(st2);
			
			while (rs.next()) {

				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {

					if (rs.getObject(i) != null) {
						su += rs.getObject(i).toString() + "\t";
					} else {
						su += "nul" + "\t";
					}

				}

				su += "\n";

			}

			System.out.println(su);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
